// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { ViewType, WaifuType } from './extension.interface';
import { getHtml, getWaifu } from './utils';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	const showWaifu = async (daily: Boolean = false) => {
		try {
			const config = vscode.workspace.getConfiguration('daily-waifu');
			const type = config.get('nsfw') ? WaifuType.NSFW : WaifuType.SFW;
			const resp = await getWaifu(type);
			const panel = initPanelView();

			if (!daily) {
				panel.webview.html = await getHtml(ViewType.Default, { waifuImageURL: resp.data.url });
				return;
			}

			panel.webview.html = await getHtml(ViewType.Daily, { waifuImageURL: resp.data.url });
		} catch (err) {
			console.error(err);
		}
	};

	const preventToday = (mode: Boolean = true) => {
		if (mode) {
			context.globalState.update('daily-waifu.lastShow', new Date().toDateString());
		} else {
			context.globalState.update('daily-waifu.lastShow', new Date(0).toDateString());
		}
	};

	const lastShow: String = new Date(
		context.globalState.get('daily-waifu.lastShow') || 0
	).toDateString();
	if (new Date().toDateString() !== lastShow) {
		showWaifu(true);
	}

	const initPanelView = () => {
		const panel = vscode.window.createWebviewPanel(
			'waifuView',
			'Waifu uwu',
			vscode.ViewColumn.One,
			{
				enableScripts: true,
			}
		);

		panel.webview.onDidReceiveMessage(
			(message) => {
				switch (message.command) {
					case 'preventToday':
						preventToday();
						return;
					case 'keepThrowing':
						preventToday(false);
						return;
				}
			},
			undefined,
			context.subscriptions
		);

		return panel;
	};

	const throwWaifuDisposable = vscode.commands.registerCommand(
		'daily-waifu.randomWaifu',
		showWaifu
	);
	const keepShowWaifuTodayDisposable = vscode.commands.registerCommand(
		'daily-waifu.keepThrowing',
		() => preventToday(false)
	);

	context.subscriptions.push(throwWaifuDisposable, keepShowWaifuTodayDisposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
