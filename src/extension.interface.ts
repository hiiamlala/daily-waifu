export enum WaifuType {
	NSFW = 'nsfw',
	SFW = 'sfw',
}

export enum ViewType {
	Daily = 'daily',
	Default = 'default',
}
