import axios from 'axios';
import Handlebars from 'handlebars';
import DailyTemplate from 'raw-loader!../template/daily.hbs';
import DefaultTemplate from 'raw-loader!../template/default.hbs';
import { API_ENDPOINT } from '../const';
import { ViewType, WaifuType } from '../extension.interface';

const apiInstance = axios.create({
	baseURL: API_ENDPOINT,
});

export const getWaifu = async (type: WaifuType) => {
	return await apiInstance.get(`${type}/waifu`);
};

export const getHtml = async (type: ViewType, data: { waifuImageURL: string }) => {
	const template = (() => {
		switch (type) {
			case ViewType.Daily:
				return DailyTemplate;
			default:
				return DefaultTemplate;
		}
	})();

	return Handlebars.compile(template)({
		waifuImageURL: data.waifuImageURL,
	});
};
